# node-phpfpm [Fork]
node.js run php scripts via phpfpm. Fork of [https://www.npmjs.com/package/node-phpfpm](https://www.npmjs.com/package/node-phpfpm)

```
npm install bitbucket:vuebix/node-phpfpm --save
```

## Usage

```js
var PHPFPM = require('node-phpfpm');

var phpfpm = new PHPFPM(
{
    host: '127.0.0.1',
    port: 9000,
    documentRoot: __dirname
});

phpfpm.run('test.php', function(output)
{
    if (output.isError) console.error('PHPFPM server error');
    if (output.errors) console.error(output.errors);
    
    console.log(output);
});
```

## Configuration

```js
var phpfpm = new PHPFPM(configObject);
```

configObject may have the following keys:


* `documentRoot` optional [string] the document root folder of PHP scripts. must end with /
* `host` optional [string] the ip or host name of php-fpm server (default: 127.0.0.1)
* `port` optional [int] the port of php-fpm server ( default: 9000 )
* `sockFile` optional [string] use the unix sock file instead of 127.0.0.1:9000 to connect php-fpm server 


## APIs

### run(options, callback)

available keys in options object


* `uri` [string] path to your phpfile
* `url` <optinal> [string] alias of uri
* `method` optional [string] GET or POST (default: GET)
* `form` optional [object] form_data that will be sent with content-type: application/x-www-form-urlencoded 
* `json` optional [object] json data that will be sent with content-type: application/json 
* `body` optional [string] raw post body data
* `contentType` optional [string] the content-type header
* `contentLength` optional [string] the content-length header
* `fastcgiParams` optional [object] additional environment parameter that will be sent to PHP-FPM


if you send a string as `options`, it will be converted to:

```js
{ uri: "the string value", method: 'GET' }
```

callback

```js
function(output)
{
    // output.isError === true, means that error occur
    // otherwise it will always equal to false
    
    // output.body is data, which php script returned
    
    // output.head store all headers, which php script returned
    
    // output.errors is the php errors detail string
    // php will output some errors, but that does not mean the request fails
    // if you turn on display_errors in your php.ini, the phpErrors content will also be found in the output.body string
    
    console.log(output); 
}
```

## Demo

Simple php request with no parameters
```js
phpfpm.run('test1.php', function(output)
{
    console.log(output);
});
```

Send data via GET method
```js
phpfpm.run('test.php?a=b&c=d&e[0]=1&e[1]=2', function(output)
{
    console.log(output);
});
```
```php
<?php
print_r($_GET);
// Array
// (
//     [a] => b
//     [c] => d
//     [e] => Array
//         (
//             [0] => 1
//             [1] => 2
//         )
// )
?>
```

Send form data via POST method
```js
phpfpm.run(
{
    uri: 'test.php',
    form: 
    {
        a:'a',
        b:'b'
    }
}, function(output)
{
    console.log(output);
});
```
```php
<?php
print_r($_POST);
// Array
// (
//     [a] => a
//     [b] => b
// )
?>
```

Send json data with POST method
```js
phpfpm.run(
{
    uri: 'test.php',
    json: 
    {
        a:'a',
        b:'b'
    }
}, function(output)
{
    console.log(output);
});
```
```php
<?php
echo file_get_contents('php://input');
// {"a":"a","b":"b"}
?>
```

Send form data with GET method
```js
phpfpm.run(
{
    uri: 'test2.php',
    method: 'GET',
    form: 
    {
        a:'a',
        b:'b'
    }
}, function(output)
{
    console.log(output);
});
```
```php
<?php
print_r($_GET);
// Array
// (
//     [a] => a
//     [b] => b
// )
?>
```

Send form data and query string with GET method
```js
phpfpm.run(
{
    uri: 'test2.php?c=cc',
    method: 'GET',
    form: 
    {
        a:'a',
        b:'b'
    }
}, function(output)
{
    console.log(output);
});
```
```php
<?php
print_r($_GET);
// Array
// (
//     [c] => cc
//     [a] => a
//     [b] => b
// )
?>
```

Send raw body data with POST method
```js
phpfpm.run(
{
    uri: 'test5.php',
    body: 'abc123'
}, function(output)
{
    console.log(output);
});
```
```php
<?php
echo file_get_contents('php://input');
// abc123
?>
```

## License
MIT

## Thanks

This project is based on the great work of `node-fastcgi-client` written by LastLeaf. [LastLeaf/node-fastcgi-client](https://github.com/LastLeaf/node-fastcgi-client)
This project also is fork on the great work of `node-phpfpmt` written by longbill (Chunlong). [longbill/node-phpfpm](https://github.com/longbill/node-phpfpm)

